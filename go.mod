module bitbucket.com/siimooo/go-starter-kit-k8

go 1.13

require (
	github.com/golang/protobuf v1.3.1
	github.com/pepeunlimited/microservice-kit v0.0.0-20191212102325-e83f82f36d67 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/twitchtv/twirp v5.7.0+incompatible

)
