package validator

import (
	"bitbucket.com/siimooo/go-starter-kit-k8/internal/app/app1/rpc"
)

type TodoServerValidator struct {}


func NewTodoServerValidator() TodoServerValidator {
	return TodoServerValidator{}
}

func (TodoServerValidator) CreateTodo(params *rpc.CreateTodoParams) error {
	return nil
}

func (TodoServerValidator) GetTodo(params *rpc.GetTodoParams) error {
	return nil
}

func (TodoServerValidator) UpdateTodo(params *rpc.UpdateTodoParams) error {
	return nil
}

func (TodoServerValidator) DeleteTodo(params *rpc.DeleteTodoParams) error {
	return nil
}