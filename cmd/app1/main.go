package main

import (
	"bitbucket.com/siimooo/go-starter-kit-k8/internal/app/app1/rpc"
	"bitbucket.com/siimooo/go-starter-kit-k8/internal/app/app1/server"
	"github.com/pepeunlimited/microservice-kit/headers"
	"github.com/pepeunlimited/microservice-kit/middleware"
	"log"
	"net/http"
)

const (
	Version = "0.1"
)

func main() {
	log.Printf("Starting the TodoServer... version=[%v]", Version)

	ts := rpc.NewTodoServiceServer(server.NewTodoServer(), nil)

	mux := http.NewServeMux()
	mux.Handle(ts.PathPrefix(), middleware.Adapt(ts, headers.XForwardedForz()))

	if err := http.ListenAndServe(":8080", mux); err != nil {
		log.Panic(err)
	}
}